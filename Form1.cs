﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace QueenApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            for (int k = 1; k < 14; k++) {
                comboBox1.Items.Add(k.ToString());
            }
            comboBox1.SelectedIndex = 7;
        }
        //設定數量
        static int Q_Number = 8;
        //棋盤大小
        static int[] queen_p = new int[Q_Number];
        //紀錄多少解
        static int Q_Solution = 0;
        //紀錄有解的陣列，用 ; 去spilt
        static string stringlist;
        //遞迴導致溢位問題改用bool判斷
        static bool backTF = false;


        private void button1_Click(object sender, EventArgs e)
        {
            clear();
            
            //歸零
            for (int i = 0; i < Q_Number; i++)
            {
                queen_p[i] = 0;
            }
            //開始執行 從0開始
            RunQueenAaary(0);
            var Qarray = stringlist.Split(';');
            Anwser1.Text = "";
            foreach (var A1 in Qarray[0].Split(','))
            {
                for (int g = 0; g < Q_Number; g++)
                {
                    Anwser1.Text += g == Convert.ToInt32(A1) ? "Q" : ".";
                }
                Anwser1.Text += "\r\n";
            }
            Anwser2.Text = "";
            foreach (var A2 in Qarray[1].Split(','))
            {
                for (int h = 0; h < Q_Number; h++)
                {
                    Anwser2.Text += h == Convert.ToInt32(A2) ? "Q" : ".";
                }
                Anwser2.Text += "\r\n";
            }
            foreach (var li in Qarray) {
                listBox1.Items.Add(li);
            }
            lab1.Text = Q_Number + "皇后問題有" + Q_Solution + "種解，目前Solution1與Solution2為前兩個解";
        }

        static void RunQueenAaary(int now)
        {

            for (int i = 0; i < Q_Number; i++)
            {
                //如果超出設定範圍，返回前一Move
                while (TF(i) || queen_p[i] >= Q_Number)
                {
                    //位置向右
                    queen_p[i]++;
                    //如果沒有結果
                    if (queen_p[i] >= Q_Number)
                    {
                        //越界→結束
                        if (queen_p[0] >= Q_Number)
                        {
                            return;
                        }
                        //當前位置歸0
                        queen_p[i] = 0;
                        //返回上一Move
                        i--;
                        //位置右移
                        queen_p[i]++;
                        backTF = true;
                        //RunQueenAaary(i);
                        //return;
                    }
                }
                //因為for的++問題 返回上層再減一次
                if (backTF == true) { i--; backTF = false; }

                //解有出現第八個&沒衝突
                if (i == Q_Number - 1)
                {
                    //寫入陣列 ,分隔 ;資料尾
                    for (int z = 0; z < queen_p.Length; z++)
                    {
                        stringlist += z == queen_p.Length - 1 ? queen_p[z] + ";" : queen_p[z] + ",";
                    }
                    //歸0
                    queen_p[i] = 0;
                    i--;
                    //位置右移
                    queen_p[i]++;
                    //有解紀錄+1
                    Q_Solution++;
                    i--;
                    //RunQueenAaary(i);
                    //return;
                }
            }
        }

        /// <summary>
        /// 當前是第now個皇后,一個皇后佔一行，所以它是第幾個皇后也就表示，這個皇后在第幾行
        /// </summary>
        /// <param name="position">位置</param>
        /// <returns></returns>
        static bool TF(int position)
        {
            for (int i = 0; i < position; i++)
            {
                //位置的上下左右與左右斜列判斷
                if (((i - position) == (queen_p[i] - queen_p[position])) || ((i - position) == -(queen_p[i] - queen_p[position])) || (queen_p[i] == queen_p[position]))
                {
                    return true;
                }
            }
            //有交疊 表示衝突
            return false;
        }
        /// <summary>
        /// 點選其他解的呈現
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            label_list.Text = "";
            foreach (var lis_lab in listBox1.SelectedItem.ToString().Split(','))
            {
                for (int h = 0; h < Q_Number; h++)
                {
                    label_list.Text += h == Convert.ToInt32(lis_lab) ? "Q" : ".";
                }
                label_list.Text += "\r\n";
            }
        }

        private void clear() {
            listBox1.Items.Clear();
            Anwser1.Text = "";
            Anwser2.Text = "";
            lab1.Text = "";
            label_list.Text = "";
            stringlist = "";
            Q_Number = Convert.ToInt32(comboBox1.SelectedItem);
            queen_p = new int[Q_Number];
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clear();
        }
    }
}
